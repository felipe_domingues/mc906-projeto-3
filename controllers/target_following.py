# Controlador para perseguicao de objetos sem desviar de obstaculos, nao incluido no relatorio
import math
import numpy as np
import skfuzzy as fuzz

from fuzzy_controller import ctrl, COMPOSITION_LARSEN, COMPOSITION_MANDANI

from math import pi


class TargetFollowingController:
    def __init__(self):
        SPEED_STEP = 0.01

        speed_left = ctrl.Consequent(np.arange(-1, 1 + SPEED_STEP, SPEED_STEP), 'speed_left')
        speed_left['fastnegative'] = fuzz.trimf(speed_left.universe, [-1 - SPEED_STEP, -1, -0.7])
        speed_left['mediumnegative'] = fuzz.trapmf(speed_left.universe, [-0.9, -0.6, -0.5, -0.2])
        speed_left['slownegative'] = fuzz.trapmf(speed_left.universe, [-0.5, -0.3, -0.2, 0])
        speed_left['stopping'] = fuzz.trimf(speed_left.universe, [-0.1, 0, 0.1])
        speed_left['slowpositive'] = fuzz.trapmf(speed_left.universe, [0, 0.2, 0.3, 0.5])
        speed_left['mediumpositive'] = fuzz.trapmf(speed_left.universe, [0.2, 0.5, 0.6, 0.9])
        speed_left['fastpositive'] = fuzz.trimf(speed_left.universe, [0.7, 1, 1 + SPEED_STEP])

        speed_right = ctrl.Consequent(np.arange(-1, 1 + SPEED_STEP, SPEED_STEP), 'speed_right')
        speed_right['fastnegative'] = fuzz.trimf(speed_right.universe, [-1 - SPEED_STEP, -1, -0.7])
        speed_right['mediumnegative'] = fuzz.trapmf(speed_right.universe, [-0.9, -0.6, -0.5, -0.2])
        speed_right['slownegative'] = fuzz.trapmf(speed_right.universe, [-0.5, -0.3, -0.2, 0])
        speed_right['stopping'] = fuzz.trimf(speed_right.universe, [-0.1, 0, 0.1])
        speed_right['slowpositive'] = fuzz.trapmf(speed_right.universe, [0, 0.2, 0.3, 0.5])
        speed_right['mediumpositive'] = fuzz.trapmf(speed_right.universe, [0.2, 0.5, 0.6, 0.9])
        speed_right['fastpositive'] = fuzz.trimf(speed_right.universe, [0.7, 1, 1 + SPEED_STEP])

        ANGLE_STEP = 0.01
        angle = ctrl.Antecedent(np.arange(-pi, pi + ANGLE_STEP, ANGLE_STEP), 'angle')
        angle['toofar_clockwise'] = fuzz.pimf(angle.universe, -pi - ANGLE_STEP, -pi, -3 * pi / 4, -pi / 2)
        angle['far_clockwise'] = fuzz.pimf(angle.universe, -3 * pi / 4, -pi / 2, -pi / 2, -pi / 4)
        angle['close_clockwise'] = fuzz.pimf(angle.universe, -pi / 2, -pi / 4, -pi / 4, 0)
        angle['avg'] = fuzz.pimf(angle.universe, -pi / 4, 0, 0, pi / 4)
        angle['close_anticlockwise'] = fuzz.pimf(angle.universe, 0, pi / 4, pi / 4, pi / 2)
        angle['far_anticlockwise'] = fuzz.pimf(angle.universe, pi / 4, pi / 2, pi / 2, 3 * pi / 4)
        angle['toofar_anticlockwise'] = fuzz.pimf(angle.universe, pi / 2, 3 * pi / 4, pi + ANGLE_STEP, pi + ANGLE_STEP)

        TARGET_DISTANCE_STEP = 0.01
        target_distance = ctrl.Antecedent(np.arange(0, 1, TARGET_DISTANCE_STEP), 'target_distance')

        target_distance['close'] = fuzz.pimf(target_distance.universe, -ANGLE_STEP, 0, 0.05, 0.2)
        target_distance['medium'] = fuzz.pimf(target_distance.universe, 0.1, 0.2, 0.3, 0.4)
        target_distance['far'] = fuzz.pimf(target_distance.universe, 0.3, 0.45, 1, 1 + TARGET_DISTANCE_STEP)

        speed_right.defuzzify_method = 'centroid'
        speed_left.defuzzify_method = 'centroid'

        self.rules = [
         # rules to adjust the angles
         ctrl.Rule(angle["toofar_clockwise"] & ~target_distance["close"], speed_left['fastpositive']),
         ctrl.Rule(angle["toofar_clockwise"] & ~target_distance["close"], speed_right['fastnegative']),

         ctrl.Rule(angle["far_clockwise"] & ~target_distance["close"], speed_left['mediumpositive']),
         ctrl.Rule(angle["far_clockwise"] & ~target_distance["close"], speed_right['mediumnegative']),

         ctrl.Rule(angle["close_clockwise"] & ~target_distance["close"], speed_left['slowpositive']),
         ctrl.Rule(angle["close_clockwise"] & ~target_distance["close"], speed_right['slownegative']),

         ctrl.Rule(angle["close_anticlockwise"] & ~target_distance["close"], speed_right['slowpositive']),
         ctrl.Rule(angle["close_anticlockwise"] & ~target_distance["close"], speed_left['slownegative']),

         ctrl.Rule(angle["far_anticlockwise"] & ~target_distance["close"], speed_right['mediumpositive']),
         ctrl.Rule(angle["far_anticlockwise"] & ~target_distance["close"], speed_left['mediumnegative']),

         ctrl.Rule(angle["toofar_anticlockwise"] & ~target_distance["close"], speed_right['fastpositive']),
         ctrl.Rule(angle["toofar_anticlockwise"] & ~target_distance["close"], speed_left['fastnegative']),

         # rules to adjust the speed towards the target
         ctrl.Rule(target_distance["far"], speed_left['mediumpositive']),
         ctrl.Rule(target_distance["far"], speed_right['mediumpositive']),

         ctrl.Rule(target_distance["medium"], speed_left['slowpositive']),
         ctrl.Rule(target_distance["medium"], speed_right['slowpositive']),

         ctrl.Rule(target_distance["close"], speed_left['stopping']),
         ctrl.Rule(target_distance["close"], speed_right['stopping']),
        ]

        self.speed_ctrl = ctrl.ControlSystem(self.rules)
        self.speed_ctrl.composition_method = COMPOSITION_MANDANI

    def calc_velocity(self, theta, d):
        speed_sim = ctrl.ControlSystemSimulation(self.speed_ctrl)

        speed_sim.input['angle'] = theta
        speed_sim.input['target_distance'] = d
        speed_sim.compute()

        return speed_sim.output['speed_right'], speed_sim.output['speed_left']


target_following_controller = TargetFollowingController()
