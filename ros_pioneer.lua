function leftMotorVelocity_callback(msg)

    -- Send the desired motor velocities to the left motor:
    sim.setJointTargetVelocity(motorLeft, msg.data)

end

function rightMotorVelocity_callback(msg)

    -- Send the desired motor velocities to the right motor:
    sim.setJointTargetVelocity(motorRight, msg.data)

end

if (sim_call_type == sim.syscb_init) then

    -- Robot name
    local RobotName = 'Pioneer_p3dx'
    robot = sim.getObjectHandle('Pioneer_p3dx')

    -- Greeting message
    sim.addStatusbarMessage('Starting robot simulation')

    -- Add a banner:
    if (pluginNotFound) then
        bannerText = "Error"
    else
        bannerText = "Pioneer_ROS_COM"
    end
    white = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 }
    red = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0.2, 0.2 }
    sim.addBanner(bannerText, 0, sim.banner_bitmapfont + sim.banner_overlay, nil, sim.getObjectAssociatedWithScript(sim.handle_self), white, red)

    -- Simulation time publisher
    clock_pub = simROS.advertise('/clock', 'rosgraph_msgs/Clock')
    simROS.publisherTreatUInt8ArrayAsString(clock_pub)

    -- Vision Sensor
    vision_sensor = sim.getObjectHandle('Vision_sensor')
    vision_sensor_pub = simROS.advertise('/vision_sensor' .. RobotName, 'sensor_msgs/Image')
    simROS.publisherTreatUInt8ArrayAsString(vision_sensor_pub)

    -- Laser Sensor
    laser_sensor = sim.getObjectHandle('fastHokuyo')
    laser_sensor_pub = simROS.advertise('/laser_sensor' .. RobotName, 'sensor_msgs/LaserScan')
    simROS.publisherTreatUInt8ArrayAsString(laser_sensor_pub)

    -- Get Left and Right motors
    motorLeft = sim.getObjectHandle('Pioneer_p3dx_leftMotor')
    motorRight = sim.getObjectHandle('Pioneer_p3dx_rightMotor')

    -- Subscribe to Twist topics coming to the /cmd_vel input, call the callback every time there is a new message
    speed_sub_left = simROS.subscribe('/cmd_velLeft' .. RobotName, 'std_msgs/Float32', 'leftMotorVelocity_callback')
    simROS.subscriberTreatUInt8ArrayAsString(speed_sub_left)

    speed_sub_right = simROS.subscribe('/cmd_velRight' .. RobotName, 'std_msgs/Float32', 'rightMotorVelocity_callback')
    simROS.subscriberTreatUInt8ArrayAsString(speed_sub_right)


    -- Detach the manipulation sphere, this will be the target for the GoToGoal behavior:
    targetObj=sim.getObjectHandle('Pioneer_p3dx_target')
    sim.setObjectParent(targetObj,-1,true)


    -- Ultrasonic sensors
    -- Got the base from the original Pioneer_p3dx lua script
    u_sensors = { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 }
    for i = 1, 16, 1 do
        u_sensors[i] = sim.getObjectHandle("Pioneer_p3dx_ultrasonicSensor" .. i)
    end
    noDetectionDist = 0.8
    maxDetectionDist = 0.15
    detect = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }

    u_sensors_pub = simROS.advertise('/ultrasonic_sensor_pub'.. RobotName, 'std_msgs/Float32MultiArray')
    simROS.publisherTreatUInt8ArrayAsString(u_sensors_pub)

    -- Target, this publisher will give the relative x,y,z,thetax,thetay,thetaz position of the target
    target_pub = simROS.advertise('/target_pub'.. RobotName, 'std_msgs/Float32MultiArray')
    simROS.publisherTreatUInt8ArrayAsString(target_pub)
    target_pos = { 0, 0, 0 }
end

if (sim_call_type == sim.syscb_actuation) then


end

if (sim_call_type == sim.syscb_sensing) then
    sim.addStatusbarMessage('Sensing...')

    -- Get the simulation time stamp
    local frame_stamp = sim.getSystemTime()

    -- Send the clock message to synchronise with ROS
    simROS.publish(clock_pub, { clock = frame_stamp })

    -- Publish the image of the front vision sensor
    local vision_sensor_image, vision_sensor_w, vision_sensor_h = sim.getVisionSensorCharImage(vision_sensor)
    local vision_sensor_data = {}
    vision_sensor_data['header'] = { seq = 0, stamp = frame_stamp, frame_id = "Vision_sensor" }
    vision_sensor_data['height'] = vision_sensor_h
    vision_sensor_data['width'] = vision_sensor_w
    vision_sensor_data['encoding'] = 'rgb8'
    vision_sensor_data['is_bigendian'] = 1
    vision_sensor_data['step'] = vision_sensor_data['width'] * 3
    vision_sensor_data['data'] = vision_sensor_image
    simROS.publish(vision_sensor_pub, vision_sensor_data)

    for i = 1, 16, 1 do
        res, dist = sim.readProximitySensor(u_sensors[i])
        if (res > 0) and (dist < noDetectionDist) then
            if (dist < maxDetectionDist) then
                dist = maxDetectionDist
            end
            detect[i] = 1 - ((dist - maxDetectionDist) / (noDetectionDist - maxDetectionDist))
        else
            detect[i] = 0
        end
    end
    simROS.publish(u_sensors_pub, {data=detect})


    local position =sim.getObjectPosition(targetObj, robot)

    simROS.publish(target_pub, {data=position})
end

if (sim_call_type == sim.syscb_cleanup) then

    -- Greeting message
    sim.addStatusbarMessage('Stop robot simulation')

    -- Properly shut down all publishers and subscribers
    simROS.shutdownPublisher(clock_pub)
    simROS.shutdownPublisher(vision_sensor_pub)
    simROS.shutdownPublisher(laser_sensor_pub)
    simROS.shutdownPublisher(target_pub)
    simROS.shutdownPublisher(u_sensors_pub)
    simROS.shutdownSubscriber(speed_sub_left)
    simROS.shutdownSubscriber(speed_sub_right)

end
