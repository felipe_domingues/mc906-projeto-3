function particlesTargetVelocities1_callback(msg)

    -- Send the desired motor velocities to the engine 1:
    sim.setScriptSimulationParameter(propellerScripts[1],'particleVelocity',msg.data)

end

function particlesTargetVelocities2_callback(msg)

    -- Send the desired motor velocities to the engine 2:
    sim.setScriptSimulationParameter(propellerScripts[2],'particleVelocity',msg.data)

end

function particlesTargetVelocities3_callback(msg)

    -- Send the desired motor velocities to the engine 3:
    sim.setScriptSimulationParameter(propellerScripts[3],'particleVelocity',msg.data)

end

function particlesTargetVelocities4_callback(msg)

    -- Send the desired motor velocities to the engine 4:
    sim.setScriptSimulationParameter(propellerScripts[4],'particleVelocity',msg.data)

end

if (sim_call_type==sim.syscb_init) then

    -- Robot name
    local RobotName=''

    -- Greeting message
    sim.addStatusbarMessage('Starting robot simulation')

    -- Add a banner:
    if (pluginNotFound) then
        bannerText="Error"
    else
        bannerText="ROS_COM"
    end
    white={1,1,1,1,1,1,1,1,1,1,1,1}
    red={0,0,0,0,0,0,0,0,0,1,0.2,0.2}
    sim.addBanner(bannerText,0,sim.banner_bitmapfont+sim.banner_overlay,nil,sim.getObjectAssociatedWithScript(sim.handle_self),white,red)

    -- Simulation time publisher
    clock_pub = simROS.advertise('/clock', 'rosgraph_msgs/Clock')
    simROS.publisherTreatUInt8ArrayAsString(clock_pub)

    -- Detatch the manipulation sphere:
    targetObj=sim.getObjectHandle('Quadricopter_target')
    sim.setObjectParent(targetObj,-1,true)

    -- Base Quadricopter
    base=sim.getObjectHandle('Quadricopter_base')

    -- Particles Simulation
    particlesAreVisible=sim.getScriptSimulationParameter(sim.handle_self,'particlesAreVisible')
    sim.setScriptSimulationParameter(sim.handle_tree,'particlesAreVisible',tostring(particlesAreVisible))
    simulateParticles=sim.getScriptSimulationParameter(sim.handle_self,'simulateParticles')
    sim.setScriptSimulationParameter(sim.handle_tree,'simulateParticles',tostring(simulateParticles))

    -- Propeller Scripts
    propellerScripts={-1,-1,-1,-1}
    for i=1,4,1 do
        propellerScripts[i]=sim.getScriptHandle('Quadricopter_propeller_respondable'..i)
    end
    heli=sim.getObjectAssociatedWithScript(sim.handle_self)

    fakeShadow=sim.getScriptSimulationParameter(sim.handle_self,'fakeShadow')
    if (fakeShadow) then
        shadowCont=sim.addDrawingObject(sim.drawing_discpoints+sim.drawing_cyclic+sim.drawing_25percenttransparency+sim.drawing_50percenttransparency+sim.drawing_itemsizes,0.2,0,-1,1)
    end

    -- Get vision sensor handler
    frontVision_sensor = sim.getObjectHandle('frontVision_sensor')

    -- Front vision sensor publisher
    frontVision_sensor_pub = simROS.advertise('/frontVision_sensor'..RobotName, 'sensor_msgs/Image')
    simROS.publisherTreatUInt8ArrayAsString(frontVision_sensor_pub)

    -- Odomety publisher, useful for mapping
    odom_base_pub = simROS.advertise('/odom_base'..RobotName, 'nav_msgs/Odometry')
    simROS.publisherTreatUInt8ArrayAsString(odom_base_pub)

    odom_targetObj_pub = simROS.advertise('/odom_targetObj'..RobotName, 'nav_msgs/Odometry')
    simROS.publisherTreatUInt8ArrayAsString(odom_targetObj_pub)

    -- Helice publisher
    odom_heli_pub = simROS.advertise('/odom_heli'..RobotName, 'nav_msgs/Odometry')
    simROS.publisherTreatUInt8ArrayAsString(odom_heli_pub)

    -- Matrix Object publisher
    odom_matrix_pub = simROS.advertise('/odom_matrix_pub'..RobotName, 'nav_msgs/Odometry')
    simROS.publisherTreatUInt8ArrayAsString(odom_matrix_pub)

    -- Subscribe to Twist topics coming to the /cmd_vel1 input, call the callback every time there is a new message
    speed_sub1 = simROS.subscribe('/cmd_velPropeller1'..RobotName, 'std_msgs/Float32', 'particlesTargetVelocities1_callback')
    simROS.subscriberTreatUInt8ArrayAsString(speed_sub1)

    speed_sub2 = simROS.subscribe('/cmd_velPropeller2'..RobotName, 'std_msgs/Float32', 'particlesTargetVelocities2_callback')
    simROS.subscriberTreatUInt8ArrayAsString(speed_sub2)

    speed_sub3 = simROS.subscribe('/cmd_velPropeller3'..RobotName, 'std_msgs/Float32', 'particlesTargetVelocities3_callback')
    simROS.subscriberTreatUInt8ArrayAsString(speed_sub3)

    speed_sub4 = simROS.subscribe('/cmd_velPropeller4'..RobotName, 'std_msgs/Float32', 'particlesTargetVelocities4_callback')
    simROS.subscriberTreatUInt8ArrayAsString(speed_sub4)

end


if (sim_call_type==sim.syscb_actuation) then



end

-- "Perceiving" the environment
if (sim_call_type==sim.syscb_sensing) then

    -- Get the simulation time stamp
    local frame_stamp = sim.getSystemTime()

    -- Send the clock message to synchronise with ROS
    simROS.publish(clock_pub, {clock = frame_stamp})

    -- Publish the image of the front vision sensor
    local frontVision_sensor_image, frontVision_sensor_w, frontVision_sensor_h = sim.getVisionSensorCharImage(frontVision_sensor)
    local frontVision_sensor_data={}
    frontVision_sensor_data['header'] = {seq = 0, stamp = frame_stamp, frame_id = "frontVision_sensor"}
    frontVision_sensor_data['height'] = frontVision_sensor_h
    frontVision_sensor_data['width'] = frontVision_sensor_w
    frontVision_sensor_data['encoding'] = 'rgb8'
    frontVision_sensor_data['is_bigendian'] = 1
    frontVision_sensor_data['step'] = frontVision_sensor_data['width']*3
    frontVision_sensor_data['data'] = frontVision_sensor_image
    simROS.publish(frontVision_sensor_pub, frontVision_sensor_data)

    -- Get the robot position, orientation and velocities
    local position = sim.getObjectPosition(base, -1)
    local rotation = sim.getObjectQuaternion(base, -1)
    local velocity_linear, velocity_angular = sim.getObjectVelocity(base)

    local base_data = {}
    base_data['header'] = {seq = 0, stamp = frame_stamp, frame_id = "odom"}
    base_data['child_frame_id'] = "base_link"
    base_data['transform'] = {}
    base_data['transform']['translation'] = {x = position[1], y = position[2], z = position[3]}
    base_data['transform']['rotation'] = {x = rotation[1], y = rotation[2], z = rotation[3], w = rotation[4]}
    simROS.sendTransform(base_data)

    -- Odometry message is necessary for the navigation on a map
    local odom_data = {}
    odom_data['header'] = {seq = 0, stamp = frame_stamp, frame_id = "odom"}
    odom_data['pose'] = {}
    odom_data['pose']['pose'] = {}
    odom_data['pose']['pose']['position'] = {x = position[1], y = position[2], z = position[3]}
    odom_data['pose']['pose']['orientation'] = {x = rotation[1], y = rotation[2], z = rotation[3], w = rotation[4]}
    odom_data['pose']['covariance'] = {0}
    odom_data['twist'] = {}
    odom_data['twist']['twist'] = {}
    odom_data['twist']['twist']['linear'] = {x = velocity_linear[1], y = velocity_linear[2], z = velocity_linear[3]}
    odom_data['twist']['twist']['angular'] = {x = velocity_angular[1], y = velocity_angular[2], z = velocity_angular[3]}
    odom_data['twist']['covariance'] = {0}
    simROS.publish(odom_base_pub, odom_data)

    -- Get the robot position, orientation and velocities
    local position_targetObj = sim.getObjectPosition(targetObj, -1)
    local rotation_targetObj = sim.getObjectQuaternion(targetObj, -1)
    local velocity_linear_targetObj, velocity_angular_targetObj = sim.getObjectVelocity(targetObj)

    -- Odometry message is necessary for the navigation on a map
    local odomtargetObj_data = {}
    odomtargetObj_data['header'] = {seq = 0, stamp = frame_stamp, frame_id = "odom_targetObj"}
    odomtargetObj_data['pose'] = {}
    odomtargetObj_data['pose']['pose'] = {}
    odomtargetObj_data['pose']['pose']['position'] = {x = position_targetObj[1], y = position_targetObj[2], z = position_targetObj[3]}
    odomtargetObj_data['pose']['pose']['orientation'] = {x = rotation_targetObj[1], y = rotation_targetObj[2], z = rotation_targetObj[3], w = rotation_targetObj[4]}
    odomtargetObj_data['pose']['covariance'] = {0}
    odomtargetObj_data['twist'] = {}
    odomtargetObj_data['twist']['twist'] = {}
    odomtargetObj_data['twist']['twist']['linear'] = {x = velocity_linear_targetObj[1], y = velocity_linear_targetObj[2], z = velocity_linear_targetObj[3]}
    odomtargetObj_data['twist']['twist']['angular'] = {x = velocity_angular_targetObj[1], y = velocity_angular_targetObj[2], z = velocity_angular_targetObj[3]}
    odomtargetObj_data['twist']['covariance'] = {0}
    simROS.publish(odom_targetObj_pub, odomtargetObj_data)

    -- Get the robot position, orientation and velocities
    local position_l = sim.getObjectPosition(heli, -1)
    local rotation_l = sim.getObjectQuaternion(heli, -1)
    local velocity_linear_l, velocity_angular_l = sim.getVelocity(heli)

    -- Odometry message is necessary for the navigation on a map
    local odomheli_data = {}
    odomheli_data['header'] = {seq = 0, stamp = frame_stamp, frame_id = "odom_heli"}
    odomheli_data['pose'] = {}
    odomheli_data['pose']['pose'] = {}
    odomheli_data['pose']['pose']['position'] = {x = position_l[1], y = position_l[2], z = position_l[3]}
    odomheli_data['pose']['pose']['orientation'] = {x = rotation_l[1], y = rotation_l[2], z = rotation_l[3], w = rotation_l[4]}
    odomheli_data['pose']['covariance'] = {0}
    odomheli_data['twist'] = {}
    odomheli_data['twist']['twist'] = {}
    odomheli_data['twist']['twist']['linear'] = {x = velocity_linear_l[1], y = velocity_linear_l[2], z = velocity_linear_l[3]}
    odomheli_data['twist']['twist']['angular'] = {x = velocity_angular_l[1], y = velocity_angular_l[2], z = velocity_angular_l[3]}
    odomheli_data['twist']['covariance'] = {0}
    simROS.publish(odom_heli_pub, odomheli_data)

    m=sim.getObjectMatrix(base,-1)

    -- Odometry message is necessary for the navigation on a map
    local odommultiarray_data = {}
    odommultiarray_data['header'] = {seq = 0, stamp = frame_stamp, frame_id = "odom_array"}
    odommultiarray_data['pose'] = {}
    odommultiarray_data['pose']['pose'] = {}
    odommultiarray_data['pose']['pose']['position'] = {x = m[1], y = m[2], z = m[3]}
    odommultiarray_data['pose']['pose']['orientation'] = {x = m[4], y = m[5], z = m[6]}
    odommultiarray_data['pose']['covariance'] = {0}
    odommultiarray_data['twist'] = {}
    odommultiarray_data['twist']['twist'] = {}
    odommultiarray_data['twist']['twist']['linear'] = {x = m[7], y = m[8], z = m[9]}
    odommultiarray_data['twist']['twist']['angular'] = {x = m[10], y = m[11], z = m[12]}
    odommultiarray_data['twist']['covariance'] = {0}
    simROS.publish(odom_matrix_pub,odommultiarray_data)

    -- Script control
    s=sim.getObjectSizeFactor(base)

    if (fakeShadow) then
        itemData={position[1],position[2],0.002,0,0,1,0.2*s}
        sim.addDrawingObjectItem(shadowCont,itemData)
    end

end


if (sim_call_type==sim.syscb_cleanup) then

    -- Greeting message
    sim.addStatusbarMessage('Stop robot simulation')
    sim.removeDrawingObject(shadowCont)

    -- Properly shut down all publishers and subscribers
    simROS.shutdownPublisher(clock_pub)
    simROS.shutdownPublisher(frontVision_sensor_pub)
    simROS.shutdownPublisher(odom_base_pub)
    simROS.shutdownPublisher(odom_targetObj_pub)
    simROS.shutdownPublisher(odom_heli_pub)
    simROS.shutdownPublisher(odom_matrix_pub)
    simROS.shutdownSubscriber(speed_sub1)
    simROS.shutdownSubscriber(speed_sub2)
    simROS.shutdownSubscriber(speed_sub3)
    simROS.shutdownSubscriber(speed_sub4)

end
