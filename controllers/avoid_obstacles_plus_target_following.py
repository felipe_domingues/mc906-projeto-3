import math
import numpy as np
import skfuzzy as fuzz

from fuzzy_controller import ctrl, COMPOSITION_LARSEN, COMPOSITION_MANDANI

from math import pi


class AvoidObstaclesTargetFollowingController:
    def __init__(self):
        SPEED_STEP = 0.01

        speed_left = ctrl.Consequent(np.arange(-1, 1 + SPEED_STEP, SPEED_STEP), 'speed_left')
        speed_left['fast_negative'] = fuzz.pimf(speed_left.universe, -1 - SPEED_STEP, -1, -1, -0.7)
        speed_left['medium_negative'] = fuzz.pimf(speed_left.universe, -0.9, -0.6, -0.5, -0.2)
        speed_left['slow_negative'] = fuzz.pimf(speed_left.universe, -0.5, -0.3, -0.2, 0)
        speed_left['stopping'] = fuzz.pimf(speed_left.universe, -0.1, 0, 0, 0.1)
        speed_left['slow_positive'] = fuzz.pimf(speed_left.universe, 0, 0.2, 0.3, 0.5)
        speed_left['medium_positive'] = fuzz.pimf(speed_left.universe, 0.2, 0.5, 0.6, 0.9)
        speed_left['fast_positive'] = fuzz.pimf(speed_left.universe, 0.7, 1, 1, 1 + SPEED_STEP)

        speed_right = ctrl.Consequent(np.arange(-1, 1 + SPEED_STEP, SPEED_STEP), 'speed_right')
        speed_right['fast_negative'] = fuzz.pimf(speed_right.universe, -1 - SPEED_STEP, -1, -1, -0.7)
        speed_right['medium_negative'] = fuzz.pimf(speed_right.universe, -0.9, -0.6, -0.5, -0.2)
        speed_right['slow_negative'] = fuzz.pimf(speed_right.universe, -0.5, -0.3, -0.2, 0)
        speed_right['stopping'] = fuzz.pimf(speed_right.universe, -0.1, 0, 0, 0.1)
        speed_right['slow_positive'] = fuzz.pimf(speed_right.universe, 0, 0.2, 0.3, 0.5)
        speed_right['medium_positive'] = fuzz.pimf(speed_right.universe, 0.2, 0.5, 0.6, 0.9)
        speed_right['fast_positive'] = fuzz.pimf(speed_right.universe, 0.7, 1, 1, 1 + SPEED_STEP)

        ANGLE_STEP = 0.01
        angle = ctrl.Antecedent(np.arange(-pi, pi + ANGLE_STEP, ANGLE_STEP), 'angle')
        angle['very_far_clockwise'] = fuzz.pimf(angle.universe, -pi - ANGLE_STEP, -pi, -3 * pi / 4, -pi / 2)
        angle['far_clockwise'] = fuzz.pimf(angle.universe, -3 * pi / 4, -pi / 2, -pi / 2, -pi / 4)
        angle['close_clockwise'] = fuzz.pimf(angle.universe, -pi / 2, -pi / 4, -pi / 4, 0)
        angle['avg'] = fuzz.pimf(angle.universe, -pi / 4, 0, 0, pi / 4)
        angle['close_anticlockwise'] = fuzz.pimf(angle.universe, 0, pi / 4, pi / 4, pi / 2)
        angle['far_anticlockwise'] = fuzz.pimf(angle.universe, pi / 4, pi / 2, pi / 2, 3 * pi / 4)
        angle['very_far_anticlockwise'] = fuzz.pimf(angle.universe, pi / 2, 3 * pi / 4, pi + ANGLE_STEP,
                                                    pi + ANGLE_STEP)

        TARGET_DISTANCE_STEP = 0.01
        target_distance = ctrl.Antecedent(np.arange(0, 1, TARGET_DISTANCE_STEP), 'target_distance')

        target_distance['close'] = fuzz.pimf(target_distance.universe, -ANGLE_STEP, 0, 0.05, 0.2)
        target_distance['medium'] = fuzz.pimf(target_distance.universe, 0.1, 0.2, 0.3, 0.4)
        target_distance['far'] = fuzz.pimf(target_distance.universe, 0.3, 0.45, 1, 1 + TARGET_DISTANCE_STEP)

        speed_right.defuzzify_method = 'centroid'
        speed_left.defuzzify_method = 'centroid'

        DISTANCE_STEP = 0.01
        distances = []
        for i in range(16):
            distance = ctrl.Antecedent(np.arange(0, 1 + DISTANCE_STEP, DISTANCE_STEP), 'distance-%d' % i)
            distance['far'] = fuzz.pimf(distance.universe, -DISTANCE_STEP, 0, 0.7, 0.8)
            distance['medium'] = fuzz.pimf(distance.universe, 0.7, 0.8, 0.85, 0.95)
            distance['close'] = fuzz.pimf(distance.universe, 0.85, 0.95, 1, 1 + DISTANCE_STEP)
            distances.append(distance)

        self.rules = [
            ctrl.Rule(angle["very_far_clockwise"] & ~target_distance["close"], speed_left['fast_positive']),
            ctrl.Rule(angle["very_far_clockwise"] & ~target_distance["close"], speed_right['fast_negative']),

            ctrl.Rule(angle["far_clockwise"] & ~target_distance["close"], speed_left['medium_positive']),
            ctrl.Rule(angle["far_clockwise"] & ~target_distance["close"], speed_right['medium_negative']),

            ctrl.Rule(angle["close_clockwise"] & ~target_distance["close"], speed_left['slow_positive']),
            ctrl.Rule(angle["close_clockwise"] & ~target_distance["close"], speed_right['slow_negative']),

            ctrl.Rule(angle["close_anticlockwise"] & ~target_distance["close"], speed_right['slow_positive']),
            ctrl.Rule(angle["close_anticlockwise"] & ~target_distance["close"], speed_left['slow_negative']),

            ctrl.Rule(angle["far_anticlockwise"] & ~target_distance["close"], speed_right['medium_positive']),
            ctrl.Rule(angle["far_anticlockwise"] & ~target_distance["close"], speed_left['medium_negative']),

            ctrl.Rule(angle["very_far_anticlockwise"] & ~target_distance["close"], speed_right['fast_positive']),
            ctrl.Rule(angle["very_far_anticlockwise"] & ~target_distance["close"], speed_left['fast_negative']),

            # rules to adjust the speed towards the target
            ctrl.Rule(target_distance["far"], speed_left['medium_positive']),
            ctrl.Rule(target_distance["far"], speed_right['medium_positive']),

            ctrl.Rule(target_distance["medium"], speed_left['slow_positive']),
            ctrl.Rule(target_distance["medium"], speed_right['slow_positive']),

            ctrl.Rule(target_distance["close"], speed_left['stopping']),
            ctrl.Rule(target_distance["close"], speed_right['stopping']),

            # rules to avoid obstacles - only the frontal sensors matter
            ctrl.Rule(
                (distances[0]['medium'] | distances[1]['medium'] | distances[2]['medium'] | distances[3]['medium']),
                speed_left['slow_positive']),
            ctrl.Rule(
                (distances[0]['medium'] | distances[1]['medium'] | distances[2]['medium'] | distances[3]['medium']),
                speed_right['slow_negative']),

            ctrl.Rule((distances[0]['close'] | distances[1]['close'] | distances[2]['close'] | distances[3]['close']),
                      speed_left['medium_positive']),
            ctrl.Rule((distances[0]['close'] | distances[1]['close'] | distances[2]['close'] | distances[3]['close']),
                      speed_right['medium_negative']),

            ctrl.Rule(
                (distances[4]['medium'] | distances[5]['medium'] | distances[6]['medium'] | distances[7]['medium']),
                speed_left['slow_negative']),
            ctrl.Rule(
                (distances[4]['medium'] | distances[5]['medium'] | distances[6]['medium'] | distances[7]['medium']),
                speed_right['slow_positive']),

            ctrl.Rule((distances[4]['close'] | distances[5]['close'] | distances[6]['close'] | distances[7]['close']),
                      speed_left['medium_negative']),
            ctrl.Rule((distances[4]['close'] | distances[5]['close'] | distances[6]['close'] | distances[7]['close']),
                      speed_right['medium_positive']),

        ]

        self.speed_ctrl = ctrl.ControlSystem(self.rules)
        self.speed_ctrl.composition_method = COMPOSITION_LARSEN

    def calc_velocity(self, theta, d, sensors_data):
        speed_sim = ctrl.ControlSystemSimulation(self.speed_ctrl)

        speed_sim.input['angle'] = theta
        speed_sim.input['target_distance'] = d

        for i, d in enumerate(sensors_data[:8]):
            speed_sim.input['distance-%d' % i] = d

        speed_sim.compute()

        return speed_sim.output['speed_right'], speed_sim.output['speed_left']


avoid_obstacles_plus_target_following_controller = AvoidObstaclesTargetFollowingController()
