# P3 de MC906A

## Implementação de controladores fuzzy para navegação em ambientes internos

## Felipe Duarte Domingues - RA 171036

Para rodar a simulação é necessário utilizar o conjunto de frameworks ROS

Arquivo principal: "ros_pioneer.py"

Jupyter Notebook com demonstração dos algoritmos fuzzy aplicados: "P3 - Fuzzy Controller.ipynb"

Arquivos .LUA disponíveis para comunicação do Pioneer P3-DX da simulação com o ROS, é necessário alterar o script do Pioneer e do sensor de infra-vermelho (FastHokuyo)