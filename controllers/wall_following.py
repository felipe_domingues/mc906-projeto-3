import numpy as np
import skfuzzy as fuzz

from fuzzy_controller import ctrl, COMPOSITION_LARSEN, COMPOSITION_MANDANI


class WallFollowingController:
    def __init__(self):
        # Distance measured in the Ultrasonic sensor
        DISTANCE_STEP = 0.01
        distance = ctrl.Antecedent(np.arange(0, 1 + DISTANCE_STEP, DISTANCE_STEP), 'distance')

        distance['very_far'] = fuzz.pimf(distance.universe, -DISTANCE_STEP, -DISTANCE_STEP, 0.0, 0.2)
        distance['far'] = fuzz.pimf(distance.universe, 0.00, 0.2, 0.2, 0.35)
        distance['medium_far'] = fuzz.pimf(distance.universe, 0.2, 0.35, 0.35, 0.5)
        distance['avg'] = fuzz.pimf(distance.universe, 0.35, 0.5, 0.5, 0.65)
        distance['medium_close'] = fuzz.pimf(distance.universe, 0.5, 0.65, 0.65, 0.8)
        distance['close'] = fuzz.pimf(distance.universe, 0.65, 0.8, 0.8, 1)
        distance['very_close'] = fuzz.pimf(distance.universe, 0.8, 1, 1 + DISTANCE_STEP, 1 + DISTANCE_STEP)

        # Speed

        SPEED_STEP = 0.01
        # in function of the max speed

        speed_left = ctrl.Consequent(np.arange(0, 1 + SPEED_STEP, SPEED_STEP), 'speed_left')
        speed_left['very_low'] = fuzz.zmf(speed_left.universe, 0, 0.1)
        speed_left['low'] = fuzz.pimf(speed_left.universe, 0.00, 0.15, 0.15, 0.3)
        speed_left['medium_low'] = fuzz.pimf(speed_left.universe, 0.2, 0.35, 0.35, 0.5)
        speed_left['avg'] = fuzz.pimf(speed_left.universe, 0.35, 0.5, 0.5, 0.65)
        speed_left['medium_high'] = fuzz.pimf(speed_left.universe, 0.5, 0.65, 0.65, 0.8)
        speed_left['high'] = fuzz.pimf(speed_left.universe, 0.7, 0.85, 0.85, 1)
        speed_left['very_high'] = fuzz.smf(speed_left.universe, 0.9, 1)

        speed_right = ctrl.Consequent(np.arange(0, 1 + SPEED_STEP, SPEED_STEP), 'speed_right')
        speed_right['very_low'] = fuzz.zmf(speed_right.universe, 0, 0.1)
        speed_right['low'] = fuzz.pimf(speed_right.universe, 0.00, 0.15, 0.15, 0.3)
        speed_right['medium_low'] = fuzz.pimf(speed_right.universe, 0.2, 0.35, 0.35, 0.5)
        speed_right['avg'] = fuzz.pimf(speed_right.universe, 0.35, 0.5, 0.5, 0.65)
        speed_right['medium_high'] = fuzz.pimf(speed_right.universe, 0.5, 0.65, 0.65, 0.8)
        speed_right['high'] = fuzz.pimf(speed_right.universe, 0.7, 0.85, 0.85, 1)
        speed_right['very_high'] = fuzz.smf(speed_right.universe, 0.9, 1)

        speed_right.defuzzify_method = 'centroid'
        speed_left.defuzzify_method = 'centroid'

        self.rules = [ctrl.Rule(distance['very_far'], speed_right['very_low']),
                      ctrl.Rule(distance['far'], speed_right['low']),
                      ctrl.Rule(distance['medium_far'], speed_right['medium_low']),
                      ctrl.Rule(distance['avg'], speed_right['avg']),
                      ctrl.Rule(distance['medium_close'], speed_right['medium_high']),
                      ctrl.Rule(distance['close'], speed_right['high']),
                      ctrl.Rule(distance['very_close'], speed_right['very_high']),

                      ctrl.Rule(distance['very_far'], speed_left['very_high']),
                      ctrl.Rule(distance['far'], speed_left['high']),
                      ctrl.Rule(distance['medium_far'], speed_left['medium_high']),
                      ctrl.Rule(distance['avg'], speed_left['avg']),
                      ctrl.Rule(distance['medium_close'], speed_left['medium_low']),
                      ctrl.Rule(distance['close'], speed_left['low']),
                      ctrl.Rule(distance['very_close'], speed_left['very_low'])
                    ]
        self.speed_ctrl = ctrl.ControlSystem(self.rules)
        self.speed_ctrl.composition_method = COMPOSITION_MANDANI

    def calc_velocity(self, d):
        _sim = ctrl.ControlSystemSimulation(self.speed_ctrl)
        _sim.input['distance'] = d
        _sim.compute()

        return _sim.output['speed_right'], _sim.output['speed_left']


wall_following_controller = WallFollowingController()
