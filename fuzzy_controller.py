import numpy as np
import matplotlib
import skfuzzy as fuzz
from skfuzzy import interp_membership, defuzz
from skfuzzy.control.controlsystem import _interp_universe_fast

ctrl = fuzz.control

# WARNING, monkey patch ahead.
# Couldn't find on the new API an easy method to use Larsen's max-prod instead of Mandani's max-min.
# This code is a monkey patch to enable the switching from Mandani to Larsen using a flag
# In the original code, the Mandani's method is hardcoded

COMPOSITION_MANDANI = np.minimum
COMPOSITION_LARSEN = np.multiply


def find_memberships(self):
    '''
    First we have to upsample the universe of self.var in order to add the
    key points of the membership function based on the activation level
    for this consequent, using the interp_universe function, which
    interpolates the `xx` values in the universe such that its membership
    function value is the activation level.
    '''
    # Find potentially new values
    new_values = []

    for label, term in self.var.terms.items():
        term._cut = term.membership_value[self.sim]
        if term._cut is None:
            continue  # No membership defined for this adjective

        # Faster to aggregate as list w/duplication
        new_values.extend(
            _interp_universe_fast(
                self.var.universe, term.mf, term._cut).tolist())

    new_universe = np.union1d(self.var.universe, new_values)

    # Initilize membership
    output_mf = np.zeros_like(new_universe, dtype=np.float64)

    # Build output membership function
    term_mfs = {}
    for label, term in self.var.terms.items():
        if term._cut is None:
            continue  # No membership defined for this adjective

        upsampled_mf = interp_membership(
            self.var.universe, term.mf, new_universe)

        term_mfs[label] = self.sim.ctrl.composition_method(term._cut,
                                                           upsampled_mf)  # <- here we load the composition method
        np.maximum(output_mf, term_mfs[label], output_mf)
    return new_universe, output_mf, term_mfs


ctrl.ControlSystem.composition_method = COMPOSITION_MANDANI  # Defaults to Mandani
ctrl.controlsystem.CrispValueCalculator.find_memberships = find_memberships
