"""
Ros node for control Pioneer_p3dx running in simulation using V-REP
"""

import rospy
import message_filters
import cv2
import numpy as np
from std_msgs.msg import Float32, Float32MultiArray
from sensor_msgs.msg import Image, LaserScan
# ROS Image message -> OpenCV2 image converter
from cv_bridge import CvBridge, CvBridgeError
#from controllers.wall_following import wall_following_controller
# from controllers.avoid_obstacles import avoid_obstacles_controller
# from controllers.target_following import target_following_controller
from controllers.avoid_obstacles_plus_target_following import avoid_obstacles_plus_target_following_controller
# from controllers.wall_following_plus_avoid_obstacles import wall_following_plus_avoid_obstacles

from math import atan


class PioneerP3DX():
    def __init__(self, name):
        # Name Robot
        self.robot = name
        # Speed publishers publishers
        self.pub_velMotorLeft = None
        self.pub_velMotorRight = None
        # Vision Sensor
        # Instantiate CvBridge
        self.bridge = CvBridge()
        self.frontResolution = []
        self.frontImage = []
        self.img = []

    def initialize(self):
        """Initialize the sample node."""
        # Provide a name for the node
        rospy.init_node("felipe_domingues_robotics", anonymous=True)

        # Give some feedback in the terminal
        rospy.loginfo("Felipe Domingues Robotics node initialization")

        # Vision Sensor
        vision_sensor = message_filters.Subscriber("vision_sensor" + self.robot, Image)

        # Ultrasonic Sensors
        ultrasonic_sensors = message_filters.Subscriber("ultrasonic_sensor_pub" + self.robot, Float32MultiArray)

        # Laser Sensors
        laser_sensor = message_filters.Subscriber("laser_sensor" + self.robot, LaserScan)

        # Target - this publisher retrieves the relative position to the target
        target = message_filters.Subscriber("target_pub" + self.robot, Float32MultiArray)

        # Wait for all topics to arrive before calling the callback
        ts_control = message_filters.ApproximateTimeSynchronizer([ultrasonic_sensors, laser_sensor, target], 1, 0.1,
                                                                 allow_headerless=True)
        #ts_camera = message_filters.TimeSynchronizer([vision_sensor], 1)

        # Register the callback to be called when all sensor readings are ready
        ts_control.registerCallback(self.control_robot)
        #ts_camera.registerCallback(self.get_vision_sensor)

        # Publish the linear and angular velocities so the robot can move
        self.pub_velMotorLeft = rospy.Publisher("cmd_velLeft" + self.robot, Float32, queue_size=1)
        self.pub_velMotorRight = rospy.Publisher("cmd_velRight" + self.robot, Float32, queue_size=1)

        # Register the callback for when the node is stopped
        rospy.on_shutdown(self.stop_robot)

        # spin() keeps python from exiting until this node is stopped
        rospy.spin()

    def control_robot(self, ultrasonic_sensors, laser_sensor, target):
        def chunks(l, n):
            """Yield successive n-sized chunks from l."""
            for i in range(0, len(l), n):
                yield l[i:i + n]

        # Decide of the motor velocities:
        from math import pi
        MAX_SPEED = 2 * pi

        points_laser = chunks(laser_sensor.ranges, 2)  # Each point is scattered in x,y,z
        # print(list(points_laser))

        x, y = target.data[0], target.data[1]
        if y==0:
            ang=0
        elif x <= 0:
            ang = (y/abs(y)) * (pi/2) + atan(-x / y)
        else:
            ang = atan(y / x)

        target_dist = np.linalg.norm(np.array((x,y)))
        print(target_dist, ang)


        u_sensors = ultrasonic_sensors.data[4:9]
        #weights = [2, 2, 2, 3, 3]
        #weights = [2, 2, 2, 3, 3]
        #weights = [1,1,1,1,1]
        #print(ultrasonic_sensors.data[4:9])

        #dist = sum((x * y for x, y in zip(u_sensors, weights))) / sum(weights)
        #dist = max(ultrasonic_sensors.data[4:9])
        #speed_right_wall, speed_left_wall = wall_following_controller.calc_velocity(dist)

        #speed_right_wall, speed_left_wall = wall_following_plus_avoid_obstacles.calc_velocity(dist, ultrasonic_sensors.data)
        #speed_right_avoid, speed_left_avoid = avoid_obstacles_controller.calc_velocity(ultrasonic_sensors.data)
        #speed_right_target, speed_left_target = target_following_controller.calc_velocity(ang, target_dist if target_dist <= 1 else 1)
        speed_right_target, speed_left_target = avoid_obstacles_plus_target_following_controller.calc_velocity(ang, target_dist if target_dist <= 1 else 1, ultrasonic_sensors.data)

        speed_right, speed_left = speed_right_target, speed_left_target
        #speed_right, speed_left = speed_right_wall, speed_left_wall

        # Never publish after the node has been shut down
        if not rospy.is_shutdown():
            print("speed: %.2f, %.2f" % (speed_left, speed_right))
            # Actually publish the message
            # That's it! Now use the readings as an input for a fuzzy controller and... finished!
            self.pub_velMotorRight.publish(Float32(speed_right * MAX_SPEED))
            self.pub_velMotorLeft.publish(Float32(speed_left * MAX_SPEED))

    def get_vision_sensor(self, frontVision_sensor):
        # Convert your ROS Image message to OpenCV2
        try:
            # Convert your ROS Image message to OpenCV2
            cv_image = self.bridge.imgmsg_to_cv2(frontVision_sensor, "bgr8")
        except CvBridgeError, e:
            print e

        (rows, cols, channels) = cv_image.shape
        # OpenCV
        self.frontResolution = [rows, cols]
        self.frontImage = cv_image
        self.img = np.array(self.frontImage, dtype=np.uint8)
        self.img.resize([self.frontResolution[0], self.frontResolution[1], 3])
        self.img = np.rot90(self.img, 2)
        self.img = np.fliplr(self.img)
        self.img = cv2.cvtColor(self.img, cv2.COLOR_RGB2BGR)

        # Show frame and exit with ESC
        cv2.imshow('Front Image', self.img)
        tecla = cv2.waitKey(5) & 0xFF
        if tecla == 27:
            pass

    def stop_robot(self):
        rospy.loginfo("Stopping robot")
        self.pub_velMotorLeft.publish(Float32(0))
        self.pub_velMotorRight.publish(Float32(0))


if __name__ == "__main__":
    robot = PioneerP3DX('Pioneer_p3dx')
    robot.initialize()
