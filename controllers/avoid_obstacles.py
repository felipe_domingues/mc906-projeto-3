# Controlador para fuga de obstaculos moveis, nao incluido no relatorio
import math
import numpy as np
import skfuzzy as fuzz

from fuzzy_controller import ctrl, COMPOSITION_LARSEN, COMPOSITION_MANDANI


class AvoidObstaclesController:
    def __init__(self):
        SPEED_STEP = 0.01

        speed_left = ctrl.Consequent(np.arange(-1, 1 + SPEED_STEP, SPEED_STEP), 'speed_left')
        speed_left['fastnegative'] = fuzz.trimf(speed_left.universe, [-1 - SPEED_STEP, -1, -0.7])
        speed_left['mediumnegative'] = fuzz.trapmf(speed_left.universe, [-0.9, -0.6, -0.5, -0.2])
        speed_left['slownegative'] = fuzz.trapmf(speed_left.universe, [-0.5, -0.3, -0.2, 0])
        speed_left['stopping'] = fuzz.trimf(speed_left.universe, [-0.1, 0, 0.1])
        speed_left['slowpositive'] = fuzz.trapmf(speed_left.universe, [0, 0.2, 0.3, 0.5])
        speed_left['mediumpositive'] = fuzz.trapmf(speed_left.universe, [0.2, 0.5, 0.6, 0.9])
        speed_left['fastpositive'] = fuzz.trimf(speed_left.universe, [0.7, 1, 1 + SPEED_STEP])

        speed_right = ctrl.Consequent(np.arange(-1, 1 + SPEED_STEP, SPEED_STEP), 'speed_right')
        speed_right['fastnegative'] = fuzz.trimf(speed_right.universe, [-1 - SPEED_STEP, -1, -0.7])
        speed_right['mediumnegative'] = fuzz.trapmf(speed_right.universe, [-0.9, -0.6, -0.5, -0.2])
        speed_right['slownegative'] = fuzz.trapmf(speed_right.universe, [-0.5, -0.3, -0.2, 0])
        speed_right['stopping'] = fuzz.trimf(speed_right.universe, [-0.1, 0, 0.1])
        speed_right['slowpositive'] = fuzz.trapmf(speed_right.universe, [0, 0.2, 0.3, 0.5])
        speed_right['mediumpositive'] = fuzz.trapmf(speed_right.universe, [0.2, 0.5, 0.6, 0.9])
        speed_right['fastpositive'] = fuzz.trimf(speed_right.universe, [0.7, 1, 1 + SPEED_STEP])

        DISTANCE_STEP = 0.01
        distances = []
        for i in range(16):
            distance = ctrl.Antecedent(np.arange(0, 1 + DISTANCE_STEP, DISTANCE_STEP), 'distance-%d' % i)
            distance['far'] = fuzz.trapmf(distance.universe, [0, 0, 0.5, 0.7])
            distance['mediumclose'] = fuzz.trimf(distance.universe, [0.5, 0.7, 0.8])
            distance['close'] = fuzz.trapmf(distance.universe, [0.7, 0.8, 0.9, 1])
            distance['tooclose'] = fuzz.trimf(distance.universe, [0.9, 1, 1])
            distances.append(distance)

        self.rules = [ctrl.Rule((distances[1]['mediumclose'] | distances[2]['mediumclose'] | distances[3]['mediumclose']),
                   speed_left['slownegative']),

         ctrl.Rule((distances[1]['close'] | distances[2]['close'] | distances[3]['close']),
                   speed_left['mediumnegative']),

         ctrl.Rule((distances[1]['tooclose'] | distances[2]['tooclose'] | distances[3]['tooclose']),
                   speed_left['fastnegative']),


         ctrl.Rule((distances[14]['mediumclose'] | distances[13]['mediumclose'] | distances[12]['mediumclose']),
                   speed_left['slowpositive']),

         ctrl.Rule((distances[14]['close'] | distances[13]['close'] | distances[12]['close']),
                   speed_left['mediumpositive']),

         ctrl.Rule((distances[14]['tooclose'] | distances[13]['tooclose'] | distances[12]['tooclose']),
                   speed_left['fastpositive']),


         ctrl.Rule((distances[0]['mediumclose'] | distances[15]['mediumclose']),
                   speed_left['slowpositive']),
         ctrl.Rule((distances[0]['mediumclose'] | distances[15]['mediumclose']),
                   speed_right['slownegative']),

         ctrl.Rule((distances[0]['close'] | distances[15]['close']),
                   speed_left['mediumpositive']),
         ctrl.Rule((distances[0]['close'] | distances[15]['close']),
                   speed_right['mediumnegative']),

         ctrl.Rule((distances[0]['tooclose'] | distances[15]['tooclose']),
                   speed_left['fastpositive']),
         ctrl.Rule((distances[0]['tooclose'] | distances[15]['tooclose']),
                   speed_right['fastnegative']),


         ctrl.Rule((distances[0]['far'] & distances[1]['far'] &
                    distances[2]['far'] & distances[3]['far'] &
                    distances[12]['far'] & distances[13]['far'] &
                    distances[14]['far'] & distances[15]['far']
                    ), speed_left['stopping']),


         ctrl.Rule((distances[4]['mediumclose'] | distances[5]['mediumclose'] | distances[6]['mediumclose']),
                   speed_right['slownegative']),

         ctrl.Rule((distances[4]['close'] | distances[5]['close'] | distances[6]['close']),
                   speed_right['mediumnegative']),

         ctrl.Rule((distances[4]['tooclose'] | distances[5]['tooclose'] | distances[6]['tooclose']),
                   speed_right['fastnegative']),


         ctrl.Rule((distances[9]['mediumclose'] | distances[10]['mediumclose'] | distances[11]['mediumclose']),
                   speed_right['slowpositive']),

         ctrl.Rule((distances[9]['close'] | distances[10]['close'] | distances[11]['close']),
                   speed_right['mediumpositive']),

         ctrl.Rule((distances[9]['tooclose'] | distances[10]['tooclose'] | distances[11]['tooclose']),
                   speed_right['fastpositive']),


         ctrl.Rule((distances[7]['mediumclose'] | distances[8]['mediumclose']),
                   speed_right['slowpositive']),
         ctrl.Rule((distances[7]['mediumclose'] | distances[8]['mediumclose']),
                   speed_left['slownegative']),

         ctrl.Rule((distances[7]['close'] | distances[8]['close']),
                   speed_right['mediumpositive']),
         ctrl.Rule((distances[7]['close'] | distances[8]['close']),
                   speed_left['mediumnegative']),

         ctrl.Rule((distances[7]['tooclose'] | distances[8]['tooclose']),
                   speed_right['fastpositive']),
         ctrl.Rule((distances[7]['tooclose'] | distances[8]['tooclose']),
                   speed_left['fastnegative']),


        ctrl.Rule((distances[4]['far'] & distances[5]['far'] &
                    distances[6]['far'] & distances[7]['far'] &
                    distances[8]['far'] & distances[9]['far'] &
                    distances[10]['far'] & distances[11]['far']
                    ), speed_right['stopping']),

        ]
        self.speed_ctrl = ctrl.ControlSystem(self.rules)
        self.speed_ctrl.composition_method = COMPOSITION_MANDANI

    def calc_velocity(self, sensors_data):
        speed_sim = ctrl.ControlSystemSimulation(self.speed_ctrl)

        for i, d in enumerate(sensors_data):
            speed_sim.input['distance-%d' % i] = d

        speed_sim.compute()

        return speed_sim.output['speed_right'], speed_sim.output['speed_left']


avoid_obstacles_controller = AvoidObstaclesController()
